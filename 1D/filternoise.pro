function filterNoise, numSamples, gaussHeight, power, const

;things checked:
; Fourier transforms in IDL are the same as us
; fixed a factor of Sqrt[2T] in sigma



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 1024 ; range, the full range will go from [0, N] U [-N, -1]
; numSamples = 100 ; number of samples to take to compute estimator of A

;noise params
; power = 1 ; power of the power law for noise
; const = 0.01 ; overall constant profile for noise

; ;filter params
; filterPower = 1 ; we use an n-th power filter
; tau = 10. / range ; characteristic decay length of high-pass

;pt source params
gaussWidth = 7; width in number of pixels
; gaussHeight = 0.5; characteristic height of noise
numGaussians = 1 ; number of Gaussians to insert



ahats = [];



for sample = 0, (numSamples - 1) do begin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GENERATE NOISE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; noise is in form J = 1 / k^b + C
    ; b = power, C = const

    ; make spectral power density in k space
    k = FINDGEN(range) - FIX(range / 2)
    specDens = 1.0 / (abs(k) + 1) ^ power + const
        ; to not get divide by zero
    specDens=unwrap(specDens, /INVERSE)
    noise = genNoise(specDens)

        ; insert Gaussians
        gaussian = gaussHeight * GAUSSIAN_FUNCTION ( ( gaussWidth - 1 ) / 2)
                ; IDL generates Gaussians of FWHM (width-1)/2
        ; startIndex = FIX( ( N_ELEMENTS(noise)) * RANDOMU(Seed, numGaussians))
                ; generates numGaussian integers between [0, noise.len]
        startIndex = (N_ELEMENTS(noise) - N_ELEMENTS(gaussian)) / 2
                ; centers Gaussian on [0] element, keeping in mind IDL's stupid convention
                ; with FFT signals ([0, N] U [-N,-1] in order) is only in freq space; here
                ; we only want to center it halfway, including the width of the Gaussian
        for i = 0, numGaussians - 1 do begin ; insert each Gaussian
            for j = 0, N_ELEMENTS(gaussian) - 1 do begin ; insert each point of Gaussian
                noise[(startIndex[i] + j) MOD N_ELEMENTS(noise)] += gaussian[j] 
                        ; cyclic-ness of signal
            endfor
        endfor

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;OPTIMAL FILTERING;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; make necessary FFTs
    kSignal = FFT(noise, /INVERSE) 
        ; note that the noise variable actually carries the signal + noise
    paddedGauss = [MAKE_ARRAY((N_ELEMENTS(kSignal) - N_ELEMENTS(gaussian)) / 2), gaussian, $
        MAKE_ARRAY(N_ELEMENTS(kSignal) - (N_ELEMENTS(kSignal) + N_ELEMENTS(gaussian)) / 2)] / gaussHeight
        ; putting the Gaussian in the middle and padding with zeroes
        ; follow the same positioning as startIndex
    kGauss = FFT(paddedGauss , /INVERSE)

    ; compute Ahat
    ahat = TOTAL(CONJ(kGauss) * kSignal / specDens) / $
        TOTAL(kGauss * CONJ(kGauss) / specDens)

    ahats = [ahats, ahat]
ENDFOR

; first entry is sigma
return, [real_part(sqrt(1 / (range * TOTAL(kGauss * CONJ(kGauss) / specDens)))), real_part(ahats)]
END
