FUNCTION genNoise, specDens

    ; assumes specDens is passed in including both signs of in format [0,N/2] U [-N/2 + 1,-1]
    ; for odd, [0, N/2 - 0.5] U [-N/2 + 0.5, -1]
    range = N_ELEMENTS(specDens)
    sigma = sqrt(specDens) / sqrt(2 * range)
    specLength = FIX((range + 2) / 2) ; special number used a lot, weird +2 handles both odd and even cases

    ; generate Gaussian noise in k space; we require f(k) = f(-k)*
    rePt = RANDOMN(SEED, specLength)
    ; rePt1 = [ rePt, REVERSE( rePt[ range - 2 * specLength + 2 : specLength - 1] )]; ensure f_k = f_(-k)^*
    rePt1 = [ rePt, REVERSE( rePt[ 1 : range - specLength] )]; ensure f_k = f_(-k)^*
    imPt = RANDOMN(SEED, specLength)
    imPt1 = [ imPt, -REVERSE( imPt[ 1 : range - specLength] )]; ensure f_k = f_(-k)^*
    knose = sigma * COMPLEX( rePt1, imPt1 ); total knoise, sorry for weird name

    ; perform the FFT
    noise = FFT(knose)
    ; stop
    RETURN, real_part(noise)
END
