FUNCTION unwrap, array, INVERSE=INV
; Unwraps to [-N/2 + 1,N/2] 
; or for odd, to [-N/2 + 0.5, N/2 - 0.5]
    len = N_ELEMENTS(array)

    if len eq 0 then begin
        print, "Empty Array!!"
    endif else begin
        if keyword_set(INV) then begin; wrap an array 
            if ((len MOD 2) eq 0) then begin
                ; wrapping an even array
                return, [array[ (len / 2 - 1): *], array[0 : (len / 2 - 2)]]
            endif else begin; wrapping an odd array
                return, [array[ (len / 2): *], array[0 : (len / 2 - 1)]]
            endelse
        endif else begin; unwrap
            if ((len MOD 2) eq 0) then begin
                return, [array[ (len / 2 + 1) : *], array[0: (len / 2)]]
            endif else begin
                return, [array[ (len / 2 + 1) : *], array[0: (len / 2)]]
            endelse
        endelse
    endelse
end
