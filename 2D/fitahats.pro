PRO fitahats, samples, gaussHeight, power, const , fileName
;binSize - size of bins in histogram
;samples - num samples over which to compute
;gaussHeight - height of gaussian signal to insert
;power - power law of spectral density
;const - constant offset in spectral density
;filename - filename under which to save

raw = filterNoise(samples, gaussHeight, power, const)
    ;get raw data
est = raw[0]
data = raw[1:N_ELEMENTS(raw) - 1]
    ; parse out estimate and data
bins=histogram(real_part(data), BINSIZE=est / 10., locations=binX)
    ;parse into bins
Gfit=gaussfit(binX,bins, coeff, NTERMS=3)
    ;4 terms, height, center, width

plot, binX, bins
oplot, binX, Gfit
; WRITE_JPEG, fileName, TVRD()
print, coeff
print, est

END
