function filternoise, numSamples, gaussHeight, power, const

;things checked:
; Fourier transforms in IDL are the same as us
; fixed a factor of Sqrt[2T] in sigma



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 256. ; range, the full range will go from [0, N] U [-N, -1]
; numSamples = 100 ; number of samples to take to compute estimator of A

;noise params
; power = 1 ; power of the power law for noise
; const = 0.01 ; overall constant profile for noise

; ;filter params
; filterPower = 1 ; we use an n-th power filter
; tau = 10. / range ; characteristic decay length of high-pass

;pt source params
gaussPixWidth = 7; width in number of pixels
; gaussHeight = 0.5; characteristic height of noise
numGaussians = 1 ; number of Gaussians to insert



ahats = []

; useful gaussian
gaussian = gaussHeight * GAUSSIAN_FUNCTION ( [( gaussPixWidth - 1 ) / 2, ( gaussPixWidth - 1 ) / 2])
    ; IDL generates Gaussians of FWHM (width-1)/2
gaussWidth = sqrt(n_elements(gaussian))
; paddedGauss = make_array(range, range, VALUE = 1E-10)
paddedGauss = dblarr(range, range)
paddedGauss[0,0] = gaussian / gaussHeight
kGauss = fft_shift(FFT(paddedGauss , /INVERSE))

; useful kvec
kx = findgen(range) - range/2
ky = kx
k = sqrt((kx ## replicate(1L,range))^2 + rotate((ky ## replicate(1L,range))^2, 1))
k[range / 2 - 1, range / 2] = 1
specDens = 0.1 / k ^ power + const

for sample = 0, (numSamples - 1) do begin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GENERATE NOISE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; make noise in k space, random every time
    noise = genNoise(specDens)
    
    ; add Gaussian
    noise += paddedGauss * gaussHeight
        ; once we have random indicies, just pad each Gaussian within loop

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;OPTIMAL FILTERING;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; make necessary FFTs
    kSignal = fft_shift(FFT(noise, /INVERSE) )
        ; note that the noise variable actually carries the signal + noise

    ; compute Ahat
    ahat = TOTAL(CONJ(kGauss) * kSignal / specDens) / TOTAL(kGauss * CONJ(kGauss) / specDens)

    ahats = [ahats, ahat]
ENDFOR

; first entry is sigma
return, [real_part(sqrt(1 / ( 2 * range^2 * TOTAL(kGauss * CONJ(kGauss) / specDens)))), real_part(ahats)]

END
