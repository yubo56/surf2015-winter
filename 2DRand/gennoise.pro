FUNCTION genNoise, specDens
; argument
;       specDens - Spectral density of noise
; return
;       noise - Noise is ensured to be fully real, and we only return real part
; generates real noise given a spectral density

; assumes specDens is passed in the sane way, [-N/2 + 1, N/2]
    range = long(sqrt(N_ELEMENTS(specDens)))
    sigma = sqrt(specDens) / sqrt( 2 * range^2 )

    temp = sigma * RANDOMN(SEED, range, range)

    if (range MOD 2 eq 0) then begin ; even cases are different b/c set unpaired to zero

        knoseRe = temp ; we can only reflect part of it, not including highest bin
        knoseRe[0,0] = (temp[0:254,0:254] + reverse(reverse(temp[0:254, 0:254]),2)) / sqrt(2)

        knoseIm = dblarr(range, range)
        knoseIm[0,0] += ((temp[0: range - 2, 0:range - 2] - reverse(reverse(temp[0: range-2, 0:range-2]),2)) / sqrt(2))
        ; we drop the outermost elements b/c otherwisre reverse(reverse) goes bonkers

        ; guarantees Re is symmetric, Im is antisymmetric about -vec-k (reverse(reverse()) is just -vec-k)
        ; such tricks are why I am not a Caltech professor yet
    endif else begin
        knoseRe = (temp + reverse(reverse(temp),2)) / sqrt(2)
        knoseIm = (temp + reverse(reverse(temp),2)) / sqrt(2)
    endelse
    knoseIm[(range - 1)/2, *] = 0
    knoseIm[*, (range - 1)/2] = 0

    knose = COMPLEX(knoseRe, knoseIm); generate full knoise

    ; perform the FFT
    noise = FFT(fft_shift(knose,/REVERSE))
    RETURN, real_part(noise)
END
