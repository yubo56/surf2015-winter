function to2d, range, index
    ; simply returns te 2D index given the side length and the 1d index
    return, [index MOD range, index / range]
end
