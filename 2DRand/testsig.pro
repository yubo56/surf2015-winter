pro testsig, numSamples, gaussHeight
; arguments 
;       numSamples - number of times to try this filtering
;       gaussHeight - height of Gaussians being inserted (width/number are fixed in program)
; computes:
;   struct:
;       sigm - estimator of sigma
;       ahats - list of estimated ahats per different random realizations of noise


;; currently off by sqrt(2), sigm is sqrt(2) too small



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 256. ; range, the full range will go from [0, N] U [-N, -1]
power = 1
const = 0.01
; constants of the white noise

;pt source params
gaussPixWidth = 7; width in number of pixels


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; useful gaussian
gaussian = GAUSSIAN_FUNCTION ( [( gaussPixWidth - 1 ) / 2, ( gaussPixWidth - 1 ) / 2])
    ; IDL generates Gaussians of FWHM (width-1)/2
gaussWidth = sqrt(n_elements(gaussian))

; make specdens
kx = findgen(range) - range/2
ky = kx
k = sqrt((kx ## replicate(1L,range))^2 + rotate((ky ## replicate(1L,range))^2, 1))
k[range / 2 - 1, range / 2] = 1
specDens = 0.1 / k ^ power + const

; store amps, sigm
sigm = 0
amps = []

; get a single added-Gaussian signal
for i=0, numSamples do begin
    noise = genNoise(specDens)
    retVal = addfunc(noise, gaussHeight * gaussian, 1)
    signal = retVal.sig

    sig2 = subtractmax(signal, gaussian, specDens)
    sigm = sig2.sigm
    print, sigm
    stop
    amps = [ amps, sig2.amp ]
endfor

hist = histogram(amps, binsize=sigm/10, locations=bins)
gfit = gaussfit(bins, hist, coeff, NTERMS=3)
plot, bins, hist
oplot, bins, gfit
; write_jpeg, 'filename.png', tvrd()
print, coeff
print, sigm
end
