function addfunc, noise, signal, numSigs
; args:
;       noise - background noise to which we add signal
;       signal - signal which we add to noise
;       numSigs - how many times we add
; return: 
;       Structure with elements
;       struct.xcoords - xcoordinates of added signals
;       struct.ycoords - ycoordinates of added signals
;       struct.signal - processed noise, including signal
;
; adds an arbitrary function to a noise numSigs times, with minimum distance
; sqrt(n_elements(signal)) / 3 units apart.
    
    range = LONG(sqrt(N_ELEMENTS(noise)))
    sizeGauss = LONG(sqrt(N_ELEMENTS(signal)))
    threshold = sizeGauss / 3
    ; threshold for closest-signal in terms of pixels; this is 2*FWHM for a Gaussian

    xcoords = [LONG(RANDOMU(SEED) * (range - sizeGauss))]
    ycoords = [LONG(RANDOMU(SEED) * (range - sizeGauss))]

    ; add first signal
    paddedGauss = dblarr(range, range)
    paddedGauss[xcoords[0], ycoords[0]] = signal
    noise = noise + paddedGauss

    for i=2, numSigs do begin
        tempX = LONG(RANDOMU(SEED) * (range - sizeGauss))
        tempY = LONG(RANDOMU(SEED) * (range - sizeGauss))
        ; generate such that won't overflow end

        ; check doesn't violate threshold
        while min(abs(xcoords - tempX)) lt threshold do tempX = LONG(RANDOMU(SEED) * (range - sizeGauss))
        while min(abs(ycoords - tempY)) lt threshold do tempY = LONG(RANDOMU(SEED) * (range - sizeGauss))

        ;log
        xcoords = [xcoords, tempX]
        ycoords = [ycoords, tempY]

        ; add signal
        paddedGauss = fltarr(range, range)
        paddedGauss[tempX, tempY] = signal
        noise = noise + paddedGauss
    endfor
    return, { xs:xcoords, ys:ycoords, sig:noise}
end
