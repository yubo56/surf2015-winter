function subtractmax, signal, kernel, specDens
; Arguments
;       signal - real signal from which to subtract kernel once, at max convolution value
;       kernel - kernel which we subtract from signal; we pad it in the program so no need for
;               sam dimensions as signal
; Return
;       Struct containing
;           struct.loc - coordinate at which we found signal
;           struct.amp - amplitude of signal subtracted
;           struct.sig - new signal (real part) after subtraction
;               * - if no signal found, amp = -1, loc = [-1,-1]
;
; convolves signal and kernel, finds the point with highest value of filter,
; computes amplitude by optimal filtering formula and subtracts out that value times
; kernel


; things commented with an extra semicolon date to before figured out idl fft convention

    ; pad Kernel
    sizeSig = LONG(sqrt(N_ELEMENTS(signal)))
    sizeKer = LONG(sqrt(N_ELEMENTS(kernel)))
    paddedKernel = dblarr(sizeSig, sizeSig)
    normKer = max(kernel)
    paddedKernel[0,0] = kernel / normKer ; should be normalized kernel

    ; get k-space forms of stuff
    kkernel = fft_shift(fft(paddedKernel, /INVERSE))
    kSignal = fft_shift(fft(signal, /INVERSE))
    ; kkernel = fft_shift(fft(paddedKernel))
    ; kSignal = fft_shift(fft(signal))

    ; convolve and find max
    ; convSig = fft(conj(fft(signal, /INVERSE)) * fft(paddedKernel, /INVERSE))
    ; convSig = fft(fft_shift(conj(kkernel) * kSignal, /REVERSE))
    convSig = convol(signal, kernel)
    temp = max(convSig, maxPos); gives 2D position, but in single index.
        ; we convert
    temp = to2d(sizeSig, maxPos)
    maxX = temp[0]
    maxY = temp[1]
    maxX -= sizeKer / 2; IDL makes kernel centered, so this is simply
        ; correction for the amount the kernel was moved
    maxY -= sizeKer / 2

    ; we note in the formula we want a form e^(2 * pi * i * vec_k * vec_(x_0)). 
    kx = (replicate(1L, sizeSig) ## findgen(sizeSig)) - (sizeSig - 1) / 2
    ky = (findgen(sizeSig) ## replicate(1L, sizeSig)) - (sizeSig - 1) / 2

    ; key formula
    amp = real_part(TOTAL(exp(-2 * !PI * complex(0,1) * ((kx * maxX) + (ky * maxY)) / sizeSig) * $
    ; amp = real_part(TOTAL(exp(2 * !PI * complex(0,1) * ((kx * maxX) + (ky * maxY)) / sizeSig) * $
        conj(kkernel) * kSignal / specDens) / TOTAL( conj(kkernel) * kkernel / $
        specDens))


    ; noise sigma, for detection threshold
    sigm = real_part(sqrt(1 / ( sizeSig^2 * TOTAL(kkernel * CONJ(kkernel) / specDens))))

    ;subtraction if passes threshold
    if amp ge 5 * sigm then begin
        newKernel = dblarr(sizeSig, sizeSig)
        newKernel[maxX, maxY] = amp * kernel
        return, {loc:[maxX, maxY], amp:amp, sig:(signal - newKernel), sigm:sigm}
    endif else begin
        return, {subtracted, loc:[-1,-1], amp:-1, sig:signal, sigm:sigm}
    endelse
end

; b=dblarr(50,50)
; a=gaussian_function([3,3])
; b[12,17] = a
