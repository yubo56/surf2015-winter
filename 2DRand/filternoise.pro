pro filterNoise, numSamples, gaussHeight, numGaussians
; arguments 
;       numSamples - number of times to try this filtering
;       gaussHeight - height of Gaussians being inserted (width/number are fixed in program)
;       numGaussians - number of Gaussians to insert and seek
; return



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 256. ; size of grid
power = 8.0 / 3
const = 0.01
; constants of the white noise

;pt source params
gaussPixWidth = 5; width in number of pixels


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; useful gaussian
gaussian = GAUSSIAN_FUNCTION ( [( gaussPixWidth - 1 ) / 2, ( gaussPixWidth - 1 ) / 2])
    ; IDL generates Gaussians of FWHM (width-1)/2
gaussWidth = sqrt(n_elements(gaussian))

; make specdens
kx = findgen(range) - range/2
ky = kx
k = sqrt((kx ## replicate(1L,range))^2 + rotate((ky ## replicate(1L,range))^2, 1))
k[range / 2 - 1, range / 2] = 1
specDens = 0.1 / k ^ power + const

; keep count of extras
numExtra = []

for sample = 0, (numSamples - 1) do begin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GENERATE NOISE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; make noise in k space, random every time
    noise = genNoise(specDens)
    
    ; add Gaussian
    retVal = addfunc(noise, gaussHeight * gaussian, numGaussians)
    signal = retVal.sig
    xcoords = retVal.xs
    ycoords = retVal.ys

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;OPTIMAL FILTERING;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; store findings
    foundX = []
    foundY = []
    amps = []

    stop

    ; subtract and get values
    repeat begin
        retVal = subtractmax(signal, gaussian, specDens)
        loc = retVal.loc
        amp = retVal.amp
        signal = retVal.sig
        ; add to foundX, foundY
        foundX = [foundX, loc[0]]
        foundY = [foundY, loc[1]]
        ; add to ahats
        amps = [amps, amp]
    endrep until amp eq -1
    ; remove the extra -1 at the end
    amps = amps[0:N_ELEMENTS(amps) - 2]
    foundX = foundX[0:N_ELEMENTS(foundX) - 2]
    foundY = foundY[0:N_ELEMENTS(foundY) - 2]

    ; for pretty printing, pad xcoords, ycoords with -1
    numPad = N_ELEMENTS(foundX) - N_ELEMENTS(xcoords)
    numExtra = [numExtra, numPad]
    if numPad gt 0 then begin
        xcoords = [xcoords, intarr(numPad) - 1]
        ycoords = [ycoords, intarr(numPad) - 1]
    endif

    print, 'Cols: X, foundX, Y, foundY, (foundX, foundY, amps) sorted by amp'
    print, transpose([[xcoords[sort(xcoords)]], [foundX[sort(foundX)]], $
        [ycoords[sort(ycoords)]], [foundY[sort(foundY)]], $
        [foundX[reverse(sort(amps))]], [foundY[reverse(sort(amps))]],$
        [amps[reverse(sort(amps))]]])
        ; print peaks in order found, then separately positions/amplitudes

    print, 'Min dist btw peaks, gaussSigma, width of gaussian Kernel'
    print, [min(distance_measure(transpose([[xcoords[uniq(xcoords)]],[ycoords[uniq(ycoords)]]]))), $
        gaussPixWidth, gaussWidth]
        ; prints min distance between peaks, sigma of gaussian, width of gauss signal

    print, 'Noise level'
    print, retVal.sigm
    stop
ENDFOR

; print, moment(numExtra)
; plot, histogram(numExtra)
; stop

END
